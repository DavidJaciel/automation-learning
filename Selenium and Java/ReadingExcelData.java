package operations;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class ReadingExcelData {

	@SuppressWarnings("resource")
	public String[] excelData() throws IOException {
		// Location of the excelFile
		String excelPath = ".\\dataFiles\\dataFile.xlsx";

		FileInputStream inputStream = new FileInputStream(excelPath);
		XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = workbook.getSheet("Hoja1");
		int rows = sheet.getLastRowNum();
		int columns = sheet.getRow(1).getLastCellNum();
		String array[] = new String[4];
		for (int r = 1; r <= rows; r++) {
			XSSFRow row = sheet.getRow(r);
			for (int c = 0; c < columns; c++) {
				XSSFCell cell = row.getCell(c);
				System.out.println(cell.getStringCellValue());
				array[c] = cell.getStringCellValue();
			}
		}
		
		return array;
	}
	
	public void takeScreenShot(String screenShotName, WebDriver driver) throws Exception {
	    File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    FileUtils.copyFile(scrFile, new File(".\\ScreenShots\\"+screenShotName+".png"));
	}
	
}
