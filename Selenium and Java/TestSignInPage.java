package operations;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestSignInPage {
	
	@SuppressWarnings({ "resource", "deprecation" })
	@Test
	public void testSignIn() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");

		WebDriverWait wait = new WebDriverWait(driver, 5);
		SignInPage signIn = new SignInPage(driver);
		ReadingExcelData data = new ReadingExcelData();
		String array[] = new String[4];
		array = data.excelData();
		signIn.firstName().sendKeys(array[0]);
		signIn.lastName().sendKeys(array[1]);
		signIn.titleJob().sendKeys(array[2]);
		signIn.educationLevel().click();
		signIn.sex().click();
		data.takeScreenShot("checkBox", driver);		//<---------- CheckBox ScreenShot
		signIn.menuYears().click();
		signIn.optionYears().click();
		data.takeScreenShot("selectOption", driver);	//<---------- Select ScreenShot
		signIn.dateMenu().sendKeys(array[3] + Keys.RETURN);
		data.takeScreenShot("date", driver);		   //<---------- Date Picker ScreenShot 	     
		signIn.submit().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		data.takeScreenShot("success", driver);		    //<---------- Success ScreenShot
		assertEquals(driver.findElement(By.xpath("//*/body/div/h1[starts-with(text(),'Thanks')]")).getText(),
			"Thanks for submitting your form");
		System.out.println("Success");
	}
}
