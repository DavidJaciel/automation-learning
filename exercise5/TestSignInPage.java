package operations;

import static org.junit.Assert.assertEquals;


import java.io.IOException;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestSignInPage {
	
	@SuppressWarnings("resource")
	@Test
	public void testSignIn() throws IOException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");

		
		
		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver, 5);
		SignInPage signIn = new SignInPage(driver);
		ReadingExcelData data = new ReadingExcelData();
		String array[] = new String[7];
		array = data.excelData();
		signIn.firstName().sendKeys(array[0]);
		signIn.lastName().sendKeys(array[1]);
		signIn.titleJob().sendKeys(array[2]);
		signIn.educationLevel().click();
		signIn.sex().click();
		signIn.menuYears().click();
		signIn.optionYears().click();
		signIn.dateMenu().sendKeys(array[6] + Keys.RETURN);
		signIn.submit().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		assertEquals(driver.findElement(By.xpath("//*/body/div/h1[starts-with(text(),'Thanks')]")).getText(),
			"Thanks for submitting your form");
		System.out.println("Success");
	}
}
