package operations;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SignInPage {
	protected WebDriver driver;

	public SignInPage(WebDriver driver) {
		this.driver = driver;
	}

	private By firstName = By.id("first-name");
	private By lastName = By.id("last-name");
	private By jobTitle = By.xpath("//*/input[@id='job-title']");
	private By educationLevel = By.id("radio-button-1");
	private By sex = By.xpath("//*/input[@id='checkbox-1']");
	private By menu = By.cssSelector("select[id='select-menu']");
	private By yearsOption = By.cssSelector("option[value='3']");
	private By dateMenu = By.cssSelector("input[id='datepicker']");
	private By submitButton = By.xpath("//*/a[contains(text(),'Submit')]");
	private By message = By.xpath("//*/body/div/h1[starts-with(text(),'Thanks')]");

	public WebElement firstName() {
		return driver.findElement(firstName);
	}

	public WebElement lastName() {
		return driver.findElement(lastName);
	}

	public WebElement titleJob() {
		return driver.findElement(jobTitle);
	}

	public WebElement educationLevel() {
		return driver.findElement(educationLevel);
	}

	public WebElement sex() {
		return driver.findElement(sex);
	}

	public WebElement menuYears() {
		return driver.findElement(menu);
	}

	public WebElement optionYears() {
		return driver.findElement(yearsOption);
	}

	public WebElement dateMenu() {
		return driver.findElement(dateMenu);
	}

	public WebElement submit() {
		return driver.findElement(submitButton);
	}

	public WebElement message() {
		return driver.findElement(message);
	}


}
