package encapsulated;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login {
	
	protected WebDriver driver;

	private By user = By.id("user-name");
	private By password = By.id("password");
	private By button = By.id("login-button");
	
	public Login(WebDriver driver) {
		this.driver=driver;
	}

	public WebElement userName() {
		return driver.findElement(user);
	}

	public WebElement passWord() {
		return driver.findElement(password);
	}
	
	public WebElement loginButton() {
		return driver.findElement(button);
	}
}
