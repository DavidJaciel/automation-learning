package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import encapsulated.MainPage;
import generalMethods.LoginMethod;
import generalMethods.TakeScreenshot;

public class FiltersTest {
	
	
	@SuppressWarnings("deprecation")
	@Test
	public void mainPageFilters() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver = new ChromeDriver();
		LoginMethod login = new LoginMethod();
		TakeScreenshot ss = new TakeScreenshot();
		driver.manage().window().maximize();
		login.loginInto(driver);
		MainPage elements = new MainPage(driver);
		elements.filters().click();
		WebDriverWait wait = new WebDriverWait(driver, 5);
		Select dropdown = new Select(elements.filters());
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("option[value=\"hilo\"]")));
		dropdown.selectByValue("hilo");
		assertEquals(driver.findElement(By.cssSelector("option[value=\"hilo\"]")).getText(), "Price (high to low)");
		ss.takeScreenShot("filters", driver);
		driver.quit();
	}
}
