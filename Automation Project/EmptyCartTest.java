package tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import encapsulated.Cart;
import generalMethods.LoginMethod;
import generalMethods.TakeScreenshot;

public class EmptyCartTest {

	@SuppressWarnings("deprecation")
	@Test
	public void tc_Cart_001() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.saucedemo.com/");
		WebDriverWait wait = new WebDriverWait(driver, 5);
		Cart elements = new Cart(driver);
		LoginMethod login = new LoginMethod();
		login.loginInto(driver);
		TakeScreenshot ss = new TakeScreenshot();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("add-to-cart-sauce-labs-backpack")));
		elements.add1().click();
		elements.add2().click();
		int quantity = Integer.parseInt(elements.quantityProducts().getText());
		System.out.println("Quantity before emptying the cart: "+quantity);
		Thread.sleep(1000);
		elements.cart().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("checkout")));
		elements.remove1().click();
		elements.remove2().click();
		try{
			quantity = Integer.parseInt(elements.quantityProducts().getText());
			System.out.println("Actual quantity:"+quantity);
		} catch (NoSuchElementException ex) {
			assertTrue(true);
			quantity=0;
			System.out.println("Quantity of products: "+quantity+"\nThe cart is empty, the test pass!");
		}
		ss.takeScreenShot("tc_Cart_001", driver);
		driver.quit();
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void tc_Cart_002() throws Exception {
		Thread.sleep(1000);
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver2 = new ChromeDriver();
		driver2.manage().window().maximize();
		driver2.get("https://www.saucedemo.com/");
		WebDriverWait wait = new WebDriverWait(driver2, 5);
		Cart elements = new Cart(driver2);
		LoginMethod login = new LoginMethod();
		login.loginInto(driver2);
		TakeScreenshot ss = new TakeScreenshot();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("add-to-cart-sauce-labs-backpack")));
		elements.add1().click();
		elements.add2().click();
		int quantity = Integer.parseInt(elements.quantityProducts().getText());
		System.out.println("Quantity before emptying the cart: "+quantity);
		Thread.sleep(1000);
		elements.cart().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("checkout")));
		elements.remove1().click();
		elements.remove2().click();
		//elements.checkout().click();
		try{
			quantity = Integer.parseInt(elements.quantityProducts().getText());
			System.out.println("Actual quantity:"+quantity);
		} catch (NoSuchElementException ex) {
			quantity=0;
		}
		try{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("continue")));
			System.out.println("The user should not be able to continue the purchase if the cart is empty");
			System.out.println("Quantity of products:"+quantity);
			assertTrue(false);
		} catch (TimeoutException ex) {
			assertTrue(true);
			System.out.println("Quantity of products: "+quantity+"\nThe cart is empty, the test pass!");
		}finally {
			ss.takeScreenShot("tc_Cart_002", driver2);
		    driver2.quit();
		}
	}
}
