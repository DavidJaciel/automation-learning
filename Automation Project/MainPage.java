package encapsulated;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MainPage {
	protected WebDriver driver;
	
	public MainPage(WebDriver driver) {
		this.driver=driver;
	}

	private By filters = By.cssSelector("select[class=\"product_sort_container\"]");
	private By cart = By.cssSelector("a[class=\"shopping_cart_link\"]");
	private By menuButton = By.id("react-burger-menu-btn");
	private By resetButton = By.id("reset_sidebar_link");
	private By quantityProducts = By.cssSelector("span[class=\"shopping_cart_badge\"]");
	private By logoutButton = By.id("logout_sidebar_link"); 
	private By name1 = By.cssSelector("a[id=\"item_0_title_link\"]");
	private By add1 = By.id("add-to-cart-sauce-labs-bike-light");
	private By remove1 = By.id("remove-sauce-labs-bike-light");
	private By add2 = By.id("add-to-cart-sauce-labs-bolt-t-shirt");
	
	public WebElement filters() {
		return driver.findElement(filters);
	}
	
	public WebElement cart() {
		return driver.findElement(cart);
	}
	
	public WebElement menuButton() {
		return driver.findElement(menuButton);
	}
	
	public WebElement resetButton() {
		return driver.findElement(resetButton);
	}
	
	public WebElement quantityProducts() {
		return driver.findElement(quantityProducts);
	}
	
	public WebElement logoutButton() {
		return driver.findElement(logoutButton);
	}
	
	public WebElement add1() {
		return driver.findElement(add1);
	}
	
	public WebElement add2(){
		return driver.findElement(add2);
	}
	
	public WebElement remove1() {
		return driver.findElement(remove1);
	}
	
	public WebElement name1() {
		return driver.findElement(name1);
	}

}
