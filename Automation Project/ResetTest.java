package tests;


import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import encapsulated.MainPage;
import generalMethods.LoginMethod;
import generalMethods.TakeScreenshot;

public class ResetTest {

	
	@Test
	public void reset() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver2 = new ChromeDriver();
		LoginMethod login = new LoginMethod();
		TakeScreenshot ss = new TakeScreenshot();
		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver2, 6);
		driver2.manage().window().maximize();
		login.loginInto(driver2);
		MainPage elements = new MainPage(driver2);
		elements.add1().click();
		elements.add2().click();
		int quantity = Integer.parseInt(elements.quantityProducts().getText());
		System.out.println("Quantity before reset: "+quantity);
		elements.menuButton().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("reset_sidebar_link")));
		elements.resetButton().click();
		try {
			elements.quantityProducts().getText();
		} catch (NoSuchElementException ex) {
			assertTrue(true);
			System.out.println("The app was reseted, the test pass!");
		}
		ss.takeScreenShot("tc_reset", driver2);
		driver2.quit();
	}

}
