package generalMethods;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadingExcel {

	@SuppressWarnings("resource")
	public String[] excelDataUsers() throws IOException {
		// Location of the excelFile
		String excelPath = ".\\dataFiles\\excel1.xlsx";
		FileInputStream inputStream = new FileInputStream(excelPath);
		XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = workbook.getSheet("Hoja1");
		int rows = sheet.getLastRowNum();
		int columns = sheet.getRow(1).getLastCellNum();
		String array[] = new String[2];
		String[] users = new String[4];
		for (int r = 1; r <= rows; r++) {
			XSSFRow row = sheet.getRow(r);
			for (int c = 0; c < columns; c++) {
				XSSFCell cell = row.getCell(c);
				array[c] = cell.getStringCellValue();
			}
			users[r - 1] = array[0];
		}
		return users;
	}

	@SuppressWarnings("resource")
	public String[] excelDataPassword() throws IOException {
		// Location of the excelFile
		String excelPath = ".\\dataFiles\\excel1.xlsx";
		FileInputStream inputStream = new FileInputStream(excelPath);
		XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = workbook.getSheet("Hoja1");
		int rows = sheet.getLastRowNum();
		int columns = sheet.getRow(1).getLastCellNum();
		String password[] = new String[2];
		for (int r = 1; r <= rows; r++) {
			XSSFRow row = sheet.getRow(r);
			for (int c = 0; c < columns; c++) {
				XSSFCell cell = row.getCell(c);
				password[c] = cell.getStringCellValue();
			}
		}
		return password;
	}
	
	@SuppressWarnings({ "resource", "rawtypes", "unchecked" })
	public List excelPersonalInformation() throws IOException {
		// Location of the excelFile
		String excelPath = ".\\dataFiles\\excel1.xlsx";
		FileInputStream inputStream = new FileInputStream(excelPath);
		XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = workbook.getSheet("Hoja2");
		int rows = sheet.getLastRowNum();
		int columns = sheet.getRow(1).getLastCellNum();
		List list = new ArrayList();
		for (int r = 1; r <= rows; r++) {
			XSSFRow row = sheet.getRow(r);
			for (int c = 0; c < columns; c++) {
				XSSFCell cell = row.getCell(c);
				list.add(c, cell);
			}
		}
		return list;
	}

	
}
