package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import encapsulated.MainPage;
import generalMethods.LoginMethod;
import generalMethods.TakeScreenshot;

public class LogoutTest {
	
	@Test
	public void logoutTest() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver = new ChromeDriver();
		LoginMethod login = new LoginMethod();
		TakeScreenshot ss = new TakeScreenshot();
		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver, 6);
		driver.manage().window().maximize();
		login.loginInto(driver);
		MainPage elements = new MainPage(driver);
		elements.menuButton().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("logout_sidebar_link")));
		assertEquals(driver.findElement(By.id("logout_sidebar_link")).getText(),"LOGOUT");
		elements.logoutButton().click();
		ss.takeScreenShot("logout", driver);
		driver.quit();
	}
}
