package encapsulated;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Cart {
	protected WebDriver driver;

	public Cart(WebDriver driver) {
		this.driver = driver;
	}

	private By cart = By.cssSelector("a[class=\"shopping_cart_link\"]");
	private By quantityProducts = By.cssSelector("span[class=\"shopping_cart_badge\"]");
	private By add1 = By.id("add-to-cart-sauce-labs-bike-light");
	private By add2 = By.id("add-to-cart-sauce-labs-bolt-t-shirt");
	private By remove1 = By.id("remove-sauce-labs-bike-light");
	private By remove2 = By.id("remove-sauce-labs-bolt-t-shirt");
	private By checkout = By.id("checkout");
	private By firstName = By.id("first-name");
	private By zipCode = By.id("postal-code");
	private By errorMessage = By.cssSelector("div[class=\"error-message-container error\"]");
	private By continueButton = By.id("continue");
	private By finish = By.id("finish");
	private By lastname = By.id("last-name");

	public WebElement cart() {
		return driver.findElement(cart);
	}

	public WebElement quantityProducts() {
		return driver.findElement(quantityProducts);
	}

	public WebElement checkout() {
		return driver.findElement(checkout);
	}

	public WebElement firstName() {
		return driver.findElement(firstName);
	}

	public WebElement lastname() {
		return driver.findElement(lastname);
	}

	public WebElement zipCode() {
		return driver.findElement(zipCode);
	}

	public WebElement errorMessage() {
		return driver.findElement(errorMessage);
	}

	public WebElement continueButton() {
		return driver.findElement(continueButton);
	}

	public WebElement finish() {
		return driver.findElement(finish);
	}

	public WebElement add1() {
		return driver.findElement(add1);
	}

	public WebElement add2() {
		return driver.findElement(add2);
	}

	public WebElement remove1() {
		return driver.findElement(remove1);
	}

	public WebElement remove2() {
		return driver.findElement(remove2);
	}
}
