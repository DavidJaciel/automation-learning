package generalMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import encapsulated.Login;

public class LoginMethod {
	
	@SuppressWarnings("deprecation")
	public void loginInto(WebDriver driver) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 6);
		Login login = new Login(driver);
		String users[] = new String[4];
		String password[] = new String[4];
		ReadingExcel data = new ReadingExcel();
		users = data.excelDataUsers();
		password = data.excelDataPassword();
		driver.get("https://www.saucedemo.com/");
		login.userName().sendKeys(users[0]);
		login.passWord().sendKeys(password[1]);
		login.loginButton().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span[class=title]")));
	}
}
