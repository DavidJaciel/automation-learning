package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import encapsulated.MainPage;
import generalMethods.LoginMethod;
import generalMethods.TakeScreenshot;

public class ItemsTest {

	@SuppressWarnings("deprecation")
	@Test
	public void itemsFuntionality() throws Exception {
		Thread.sleep(2000);
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver = new ChromeDriver();
		LoginMethod login = new LoginMethod();
		TakeScreenshot ss = new TakeScreenshot();
		WebDriverWait wait = new WebDriverWait(driver, 6);
		driver.manage().window().maximize();
		login.loginInto(driver);
		MainPage elements = new MainPage(driver);
		elements.add1().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("remove-sauce-labs-bike-light")));
		elements.remove1().click();
		elements.name1().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(), 'Add to cart')]")));
		assertEquals(driver.findElement(By.xpath("//*[contains(text(), 'Add to cart')]")).getText(), "ADD TO CART");
		ss.takeScreenShot("tc_items", driver);
		driver.quit();
	}
}
