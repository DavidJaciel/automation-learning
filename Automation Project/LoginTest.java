package tests;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import encapsulated.Login;
import generalMethods.ReadingExcel;
import generalMethods.TakeScreenshot;

public class LoginTest {


	ReadingExcel data = new ReadingExcel();
	
	@SuppressWarnings("deprecation")
	@Test 
	public void testCase001Login() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver = new ChromeDriver();
		WebDriverWait wait = new WebDriverWait(driver, 6);
		Login login = new Login(driver);
		String users[] = new String[4];
		String password[] = new String[4];
		TakeScreenshot ss = new TakeScreenshot();
		users = data.excelDataUsers();
		password = data.excelDataPassword();
		driver.manage().window().maximize();
		driver.get("https://www.saucedemo.com/");
		login.userName().sendKeys(users[0]);
		login.passWord().sendKeys(password[1]);
		login.loginButton().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span[class=title]")));
		ss.takeScreenShot("tc_login_001", driver);		    //<---------- Success ScreenShot
		assertEquals(driver.findElement(By.cssSelector("span[class=title]")).getText(),"PRODUCTS");
		driver.quit();
	}
	
	@SuppressWarnings("deprecation")
	@Test 
	public void testCase002Login() throws Exception {
		Thread.sleep(2000);
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver2 = new ChromeDriver();
		WebDriverWait wait = new WebDriverWait(driver2, 6);
		Login login = new Login(driver2);
		String users[] = new String[4];
		String password[] = new String[4];
		TakeScreenshot ss = new TakeScreenshot();
		users = data.excelDataUsers();
		password = data.excelDataPassword();
		driver2.manage().window().maximize();
		driver2.get("https://www.saucedemo.com/");
		login.userName().sendKeys(users[0]);
		login.passWord().sendKeys("xxxxxxxx"+password[0]);
		login.loginButton().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h3[data-test=\"error\"]")));
		ss.takeScreenShot("tc_login_002", driver2);		    //<---------- Success ScreenShot
		assertEquals(driver2.findElement(By.cssSelector("h3[data-test=\"error\"]")).getText(),
				"Epic sadface: Username and password do not match any user in this service");
		driver2.quit();
	}
	
	@SuppressWarnings("deprecation")
	@Test 
	public void testCase003Login() throws Exception {
		Thread.sleep(2000);
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver3 = new ChromeDriver();
		WebDriverWait wait = new WebDriverWait(driver3, 6);
		Login login = new Login(driver3);
		String users[] = new String[4];
		String password[] = new String[4];
		TakeScreenshot ss = new TakeScreenshot();
		users = data.excelDataUsers();
		password = data.excelDataPassword();
		driver3.manage().window().maximize();
		driver3.get("https://www.saucedemo.com/");
		login.userName().sendKeys("xxxxxx"+users[1]);
		login.passWord().sendKeys(password[1]);
		login.loginButton().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h3[data-test=\"error\"]")));
		ss.takeScreenShot("tc_login_003", driver3);		    //<---------- Success ScreenShot
		assertEquals(driver3.findElement(By.cssSelector("h3[data-test=\"error\"]")).getText(),
				"Epic sadface: Username and password do not match any user in this service");
		driver3.quit();
	}
	
	
	@SuppressWarnings("deprecation")
	@Test 
	public void testCase004Login() throws Exception {
		Thread.sleep(2000);
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver4 = new ChromeDriver();
		WebDriverWait wait = new WebDriverWait(driver4, 6);
		Login login = new Login(driver4);
		String users[] = new String[4];
		String password[] = new String[4];
		TakeScreenshot ss = new TakeScreenshot();
		users = data.excelDataUsers();
		password = data.excelDataPassword();
		driver4.manage().window().maximize();
		driver4.get("https://www.saucedemo.com/");
		login.userName().sendKeys("xxxxxx"+users[1]);
		login.passWord().sendKeys("xxxxxx"+password[1]);
		login.loginButton().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h3[data-test=\"error\"]")));
		ss.takeScreenShot("tc_login_004", driver4);		    //<---------- Success ScreenShot
		assertEquals(driver4.findElement(By.cssSelector("h3[data-test=\"error\"]")).getText(),
				"Epic sadface: Username and password do not match any user in this service");
		driver4.quit();
	}

	

}
