package tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import encapsulated.Cart;
import generalMethods.LoginMethod;
import generalMethods.ReadingExcel;
import generalMethods.TakeScreenshot;

public class CartTest {

	@SuppressWarnings({ "rawtypes", "deprecation" })
	@Test
	public void tc_Cart_003() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.saucedemo.com/");
		WebDriverWait wait = new WebDriverWait(driver, 5);
		Cart elements = new Cart(driver);
		LoginMethod login = new LoginMethod();
		login.loginInto(driver);
		TakeScreenshot ss = new TakeScreenshot();
		ReadingExcel data = new ReadingExcel();
		List listInfo = new ArrayList();
		listInfo = data.excelPersonalInformation();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("add-to-cart-sauce-labs-backpack")));
		elements.add1().click();
		elements.add2().click();
		Thread.sleep(1000);
		elements.cart().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("checkout")));
		elements.checkout().click();
		elements.firstName().sendKeys(listInfo.get(0).toString());
		elements.zipCode().sendKeys(listInfo.get(2).toString());
		Thread.sleep(1000);
		elements.continueButton().click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("div[class=\"error-message-container error\"]")));
		assertEquals(elements.errorMessage().getText(), "Error: Last Name is required");
		ss.takeScreenShot("tc_Cart_003", driver);
		driver.quit();
	}

	@SuppressWarnings({ "rawtypes", "deprecation" })
	@Test
	public void tc_Cart_004() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\david\\Documents\\WebDriver for Chrome\\ChromeDriver.exe");
		WebDriver driver2 = new ChromeDriver();
		Thread.sleep(1000);
		driver2.manage().window().maximize();
		driver2.get("https://www.saucedemo.com/");
		WebDriverWait wait = new WebDriverWait(driver2, 5);
		Cart elements = new Cart(driver2);
		LoginMethod login = new LoginMethod();
		login.loginInto(driver2);
		TakeScreenshot ss = new TakeScreenshot();
		Actions action = new Actions(driver2);
		ReadingExcel data = new ReadingExcel();
		List listInfo = new ArrayList();
		listInfo = data.excelPersonalInformation();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("add-to-cart-sauce-labs-backpack")));
		elements.add1().click();
		elements.add2().click();
		Thread.sleep(1000);
		elements.cart().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("checkout")));
		elements.checkout().click();
		elements.firstName().sendKeys(listInfo.get(0).toString());
		elements.lastname().sendKeys(listInfo.get(1).toString());
		elements.zipCode().sendKeys(listInfo.get(2).toString());
		Thread.sleep(1000);
		elements.continueButton().click();
		elements.finish().click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2[class=\"complete-header\"]")));
		assertEquals(driver2.findElement(By.cssSelector("h2[class=\"complete-header\"]")).getText(), "THANK YOU FOR YOUR ORDER");
		action.sendKeys(Keys.PAGE_UP).build().perform();
		Thread.sleep(1000);
		ss.takeScreenShot("tc_Cart_004", driver2);
		driver2.quit();
	}
}
